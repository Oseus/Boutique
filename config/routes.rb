Rails.application.routes.draw do
  root to: 'items#index'
  
  get 'profil', to: 'users#show', as: 'my_profil'
  
  get 'items/:id', to: 'items#show', as: 'item'
  
  post 'cart/add/:id', to: 'carts#add_to_cart', as: 'add_item_to_cart'
  post 'cart/delete/:id', to: 'carts#delete_of_cart', as: 'delete_item_of_cart'
  post 'cart/payment', to: 'carts#payment', as: 'cart_payment'
  post 'merci', to: 'orders#merci', as: 'merci'
  get 'cart', to: 'carts#show', as: 'cart_list'

  # get 'users/edit', to: 'users#edit', as: 'user_edit'

  post 'order/add/:id', to: 'orders#add_to_order', as: 'add_cart_to_order'
  post 'order/delete/:id', to: 'orders#delete_order', as: 'delete_order'
  get 'orders/new', to: 'orders#new', as: 'new_order'
  get 'orders/:id', to: 'orders#show', as: 'order'










  # edit_user_registration_path 	GET 	/users/edit(.:format) 	

  resources :orders
  resources :carts

  devise_for :users , except: [:edit]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
