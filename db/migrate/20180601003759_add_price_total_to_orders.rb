class AddPriceTotalToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :price_total, :float
  end
end
