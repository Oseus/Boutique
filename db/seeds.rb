# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'
# Creating 20 fake cat name, description, price, and url

Item.destroy_all


1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://parade.condenast.com/wp-content/uploads/2014/06/garfield-the-cartoon-ctr.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'https://lh3.googleusercontent.com/-vS2Whp_bOi4/WufGwbrVtRI/AAAAAAAF-_A/PjJDMj5pij8JPND6inftluNx5_1NWkLPgCJoC/w530-h680-n/.facebook_1525139094268.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://printablegraphics.in/wp-content/uploads/2017/11/Image-de-chat-3.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://chainimage.com/images/fond-ecran-chat-21.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'https://static.lexpress.fr/medias_10461/w_2048,h_890,c_crop,x_0,y_109/w_1520,h_855,c_fill,g_north/v1434031082/un-chat-siamois_5356043.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://img06.deviantart.net/9ae0/i/2011/158/0/2/bebe_chat__by_kittieloup-d3i9p79.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://rainman.pagesperso-orange.fr/slides/Chat%20Espagnol.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://miriadna.com/desctopwalls/images/max/White-cat-with-yellow-eyes.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://images4.fanpop.com/image/photos/16000000/Beautiful-Cat-cats-16096437-1280-800.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://images2.fanpop.com/image/photos/13400000/Cat-cats-13494053-1600-1200.jpg')
end

1.times do
  name = Item.create(title: Faker::Cat.name, description: Faker::Cat.breed, price: Faker::Commerce.price, image_url: 'http://fc01.deviantart.net/fs71/i/2013/139/6/1/scottish_fold_cat_by_vorchuniya-d65thwe.jpg')
end