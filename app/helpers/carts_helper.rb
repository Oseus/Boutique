module CartsHelper
	def check_total
		return current_user.cart.added_items.inject(0) { |sum, n| sum + n.price }
	end
end
