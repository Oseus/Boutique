class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :firstname, :name, presence: true, format: { with: /\A[-a-zA-Z]*\z/ }

  has_one :cart
  has_many :added_items, through: :cart

  has_many :orders
  has_many :purchased_items, through: :orders

  after_create :create_account

  def create_account
    user = User.last
    cart = Cart.new
    user.cart = cart
    user.save
    cart.save
  end

end
