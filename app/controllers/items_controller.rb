class ItemsController < ApplicationController
  def index
  	# Je récupère la liste de tous les items
  	@list_items = Item.all
  	# Sur la page index, je vais faire un each pour afficher chaque item
  end

  def show
  	@this_item = Item.find_by(params.permit(:id))
  end
end
