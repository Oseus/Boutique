class CartsController < ApplicationController

  # !!!! Genéré par Scaffold !!!

  before_action :authenticate_user! 
  before_action :set_cart, only: [:show, :edit, :update, :destroy]

  # GET /carts
  # GET /carts.json
  def index
    @carts = Cart.all
  end

  # GET /carts/1
  # GET /carts/1.json
  def show
    if current_user.cart.added_items.length > 0
      @items_of_users_cart = current_user.cart.added_items.all
    else
      @items_of_users_cart = nil
    end
  end

  # GET /carts/new
  def new
    @cart = Cart.new
  end

# GET /carts/payment
  def payment
    # @test = Item.all
    # Code stripe qui suit non nécessaire, ça se passe dans la view
  end

  # GET /carts/1/edit
  def edit
  end

  # POST /carts
  # POST /carts.json
  def create
    @cart = Cart.new(cart_params)

    respond_to do |format|
      if @cart.save
        format.html { redirect_to @cart, notice: 'Cart was successfully created.' }
        format.json { render :show, status: :created, location: @cart }
      else
        format.html { render :new }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    respond_to do |format|
      if @cart.update(cart_params)
        format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart }
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy
    respond_to do |format|
      format.html { redirect_to carts_url, notice: 'Cart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_to_cart
    the_item = Item.find_by(params.permit(:id))
    if current_user.cart.added_items.length > 0
      unless current_user.cart.added_items.find_by_id(the_item)
          current_user.cart.added_items.push(the_item)
          redirect_to cart_list_path
      else
        #Mettre alarm
      end
    else
      current_user.cart.added_items.push(the_item)
      redirect_to cart_list_path
    end
    # TODO : rajouter alarm
  end

  def merci
  end

  def delete_of_cart
    the_item = Item.find_by(params.permit(:id))
    current_user.cart.added_items.delete(the_item)
    redirect_to cart_path(current_user.id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = Cart
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.fetch(:cart, {})
    end
end
