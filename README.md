# Bonjour à tous, "tout" est là

## Introduction

Projet : boutique e-commerce

Faire un site en équipe pendant une petite semaine.

Dans ce projet, nous allons vous apprendre à réaliser un site complexe, le mettre en ligne, et l'améliorer. En une semaine. L'objectif est simple : ce projet nous permet de vous donner une marche à suivre qui est très efficace pour mettre en ligne un site rapidement : la méthode agile. Ainsi, nous allons vous donner un site à réaliser, un MVP de ce site, et une fois le MVP en ligne et fonctionnel, vous demander de coder des fonctionnalités supplémentaires. Pour la semaine prochaine, vous serez rôdés sur plein d'aspects, et vous ferez votre projet final dans les meilleures conditions possibles.


## lien heroku : 

## Petit Bilan :

- [x] MVP
- [ ] Features additionnelles

Nous n'avons pas tout reussi a faire, désolé :cry:

:pray: Indulgence required, we did our best :smile:

## La Team :heart:

**_ Hugo, David & Odyssey & Cyril & Charles_**

## Construit avec :

* Atom/VS
* et la super team #elonmuskrew

## Contributors with peer-learning : :love_letter:

* Mohammad-Ali: https://github.com/mohammadali-bacha
* Odssey: https://github.com/Odssey
* oseus: https://github.com/oseus
* Hug-O: https://github.com/Hug-O
* Cyril: https://github.com/C83
* Charles: https://github.com/CH4r135QU3NUM

## Slack des contributeurs :

* Mohammad-Ali : @Mohammad-Ali
* Odyssée Levine : @Odyssée Levine
* David Coat : @Coat David
* Hugo : @Hugo
* Cyril : @Cyril 
* Charles: @charlesrouen 
